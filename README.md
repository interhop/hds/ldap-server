# ldap-server

This provides a complete ldap server for a given organization


# Administration


## Search into a ldap server

```bash
docker exec my-openldap-container ldapsearch -x -H ldap://0.0.0.0:389 -b dc=example,dc=org -D "cn=admin,dc=example,dc=org" -w admin
```

## Add a user

```bash
docker exec my-openldap-container ldapadd -x -D "cn=admin,dc=example,dc=org" -w admin -f /container/service/slapd/assets/test/new-user.ldif -H ldap://0.0.0.0:389
```

## User template

```
dn: uid=billy,dc=example,dc=org
uid: billy
cn: billy
sn: 3
objectClass: top
objectClass: posixAccount
objectClass: inetOrgPerson
loginShell: /bin/bash
homeDirectory: /home/billy
uidNumber: 14583102
gidNumber: 14564100
userPassword: {SSHA}j3lBh1Seqe4rqF1+NuWmjhvtAni1JC5A
mail: billy@example.org
gecos: Billy User
```


## Delete a user

```bash
docker exec openldap ldapdelete -x -D "cn=admin,dc=example,dc=org" -w admin "uid=nico,dc=example,dc=org" -H ldap://0.0.0.0:389
```

## Generate a password

The salt is contained in the result, so multiplle call returns multiple results.

```bash
# into the docker container
slappasswd -h {SSHA} -s abcd123
```

